const config_sql = require('../Config/mysql.json');
const mysql = require('mysql2/promise');
const bluebird = require('bluebird');
    
module.exports = {
    get_connection : function get_connection(){
        var connection = mysql.createConnection({
            host:config_sql.host,
            user:config_sql.user,
            password:config_sql.password,
            database:config_sql.database,
            port:config_sql.db_port,
            Promise:bluebird
        });
        return connection;
    },
    insert_into_movie_db : async function(movieName, movieLink, subtitleLink, posterLink){
        var connection = await this.get_connection();
        var sql = "INSERT INTO movies (movieName, movieLink, subtitleLink, posterLink) VALUES ('"+movieName+"', '"+movieLink+"', '"+subtitleLink+"','"+posterLink+"')";
        var [result,fields] = await connection.execute(sql);
        connection.end();
        return [result,fields];
    },
    get_all_movies: async function(){
        var connection = await this.get_connection();
        var [result,fields] = await connection.execute("SELECT * FROM movies ORDER BY movieName ASC");
        connection.end();
        return [result,fields];
    },
    get_movies_by_id: async function(movie_id){
        var connection = await this.get_connection();
        var [result,fields] = await connection.execute("SELECT * FROM movies WHERE movieId='"+movie_id+"'");
        connection.end();
        return [result,fields];

    }
}