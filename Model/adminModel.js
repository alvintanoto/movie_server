const config_sql = require('../Config/mysql.json');
const mysql = require('mysql2/promise');
const bluebird = require('bluebird');
    
module.exports = {
    get_connection : function get_connection(){
        var connection = mysql.createConnection({
            host:config_sql.host,
            user:config_sql.user,
            password:config_sql.password,
            database:config_sql.database,
            port:config_sql.db_port,
            Promise:bluebird
        });
        return connection;
    },
    get_all_data : async function(){
        var connection = await this.get_connection();

        var [result,fields] = await connection.execute("SELECT * FROM admin");
        connection.end();
        return [result,fields];
    }
}