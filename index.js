const express = require('express');
const app = express();
const port = 3000;
var fs = require('fs');

app.set('view_engine', 'ejs');
app.use('/', express.static(__dirname));

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());

var adminController = require('./Controller/admin');
var movieController = require('./Controller/movies');

ensureExists(__dirname +'/Assets/Movies', 0744, function(err) {
    if (err) {
        console.log(err);
    } else {
        
    }
});

function ensureExists(path, mask, cb) {
    if (typeof mask == 'function') { // allow the `mask` parameter to be optional
        cb = mask;
        mask = 0777;
    }
    fs.mkdir(path, mask, function(err) {
        if (err) {
            if (err.code == 'EEXIST') cb(null); // ignore the error if the folder already exists
            else cb(err); // something else went wrong
        } else cb(null); // successfully created folder
    });
}

//2 parameter, route, data
app.use('/', movieController);
app.use('/', adminController);
app.get('/', (req,res)=>res.redirect('login'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))