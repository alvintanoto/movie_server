var express = require("express")
var session  = require('express-session');

var router = express.Router();

var flash    = require('connect-flash');
var movieModel = require('./../Model/movieModel');

var passport = require('passport');
require('./passport')(passport);

var multer  = require('multer')

const storage = multer.diskStorage({
    destination:function(req,filee,cb){
        cb(null, './Assets/Movies');
    },
    filename: function(req,file,cb){
        cb(null, file.originalname);
    }
});

const upload = multer({
    storage:storage
}).any();

// fields([
//     {name: 'movie_name'},
//     {name: 'movie_file'},
//     {name: 'subtitle_file'},
//     {name: 'poster_file'}
// ])

router.use(session({
	secret: '715f3cf44f14f661c3e73ccf64903cb9',
	resave: true,
	saveUninitialized: true
 } )); // session secret
router.use(passport.initialize());
router.use(passport.session()); // persistent login sessions
router.use(flash()); // use connect-flash for flash messages stored in session

router.get('/add_movie', function(req, res){
    if(req.isAuthenticated()){
        res.render('add_movie.ejs', {user:req.user, active:'add_movie'});
    }else{
        res.redirect('/login');
    }
});

router.post('/add_movie',async function(req,res){
    await upload(req,res, async function(err){
        if(err){
            console.log(err);
        }else{
            console.log(req.files);
            var movieName = req.files[0].destination+"/"+req.files[0].filename;
            movieName = movieName.substring(1);
            var movieSub = req.files[1].destination+"/"+req.files[1].filename;
            movieSub = movieSub.substring(1);
            var moviePoster = req.files[2].destination+"/"+req.files[2].filename;
            moviePoster = moviePoster.substring(1);
            await movieModel.insert_into_movie_db(req.body.movie_name, movieName, movieSub,moviePoster);
        }
    })

    //masukin path ke database
    res.redirect('/add_movie');
});

router.get('/movie/:id', async function(req,res){
    if(req.isAuthenticated()){
        console.log(req.params.id);
        let movie = await movieModel.get_movies_by_id(req.params.id);
        res.render('watch_movie.ejs', {active:"", user:req.user, movies:movie[0]});
    }else{
        res.redirect('/login');
    }
});

module.exports = router;