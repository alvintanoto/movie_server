var express = require("express")
var session  = require('express-session');
var router = express.Router();
var flash    = require('connect-flash');
var movieDb = require('./../Model/movieModel');
var passport = require('passport');
require('./passport')(passport);

router.use(session({
	secret: '715f3cf44f14f661c3e73ccf64903cb9',
	resave: true,
	saveUninitialized: true
 } )); // session secret
router.use(passport.initialize());
router.use(passport.session()); // persistent login sessions
router.use(flash()); // use connect-flash for flash messages stored in session

router.get('/login', function(req, res){
    if(req.isAuthenticated()){
        res.redirect('/dashboard');
    }else{
        res.render('login.ejs', { message: req.flash('loginMessage') });
    }
});

router.post('/login', passport.authenticate('local-login', {
    successRedirect : '/dashboard', // redirect to the secure profile section
    failureRedirect : '/login', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}), function(req, res){
    if (req.body.remember) {
      req.session.cookie.maxAge = 1000 * 60 * 3;
    } else {
      req.session.cookie.expires = false;
    }
    res.redirect('/');
});

router.get('/dashboard', loggedIn, async function(req,res){
    var allMovie = await movieDb.get_all_movies();
    res.render('dashboard.ejs', {user:req.user, active:'dashboard', movies:allMovie[0]});
});

router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/login');
  });

function loggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/login');
    }
}

module.exports = router;